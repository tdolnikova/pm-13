package com.dolnikova.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;

public final class HashUtil {

    @Nullable
    public static String stringToHashString(@Nullable final String string) {
        @NotNull String hashString = "";
        try {
            @Nullable final MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(string.getBytes());
            @Nullable final byte[] bytes = md.digest();
            hashString = DatatypeConverter.printHexBinary(bytes);
        } catch (Exception e) {e.printStackTrace();}
        return hashString;
    }

}
