package com.dolnikova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractEntity {
    @Nullable private Long date;
    @Nullable private String userId;
    @Nullable private String signature;
}
