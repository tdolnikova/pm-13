package com.dolnikova.tm.entity;

import com.dolnikova.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity implements Serializable {

    @Nullable
    private String id;
    @Nullable
    private String ownerId;
    @Nullable
    private String projectId;
    @Nullable
    private String name;
    @Nullable
    private String description;
    @Nullable
    private Date creationDate;
    @Nullable
    private Date startDate;
    @Nullable
    private Date endDate;
    @Nullable
    private Status status;

    public Task(@Nullable final String ownerId, @Nullable final String name) {
        this.name = name;
        id = UUID.randomUUID().toString();
        this.ownerId = ownerId;
        status = Status.PLANNED;
        creationDate = new Date();
        description = "some text you need to find";
    }

}
