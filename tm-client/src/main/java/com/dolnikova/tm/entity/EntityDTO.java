package com.dolnikova.tm.entity;

import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.endpoint.Task;
import com.dolnikova.tm.endpoint.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name = "EntityDTO")
public final class EntityDTO implements Serializable {
    private static final long serialVersionUID = 1;
    @Nullable private List<User> users;
    @Nullable private List<Project> projects;
    @Nullable private List<Task> tasks;
}