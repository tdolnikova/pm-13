package com.dolnikova.tm.command;

import com.dolnikova.tm.api.ServiceLocator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractCommand {

    @Nullable
    public ServiceLocator serviceLocator;
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
    @NotNull public abstract String command();
    @NotNull public abstract String description();
    public abstract void execute() throws Exception;
    public abstract boolean isSecure();
}