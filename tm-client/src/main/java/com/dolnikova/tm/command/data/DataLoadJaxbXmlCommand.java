package com.dolnikova.tm.command.data;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.endpoint.Task;
import com.dolnikova.tm.endpoint.User;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class DataLoadJaxbXmlCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return Command.DATA_LOAD_JAXB_XML;
    }

    @Override
    public @NotNull String description() {
        return Command.DATA_LOAD_JAXB_XML_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        List<Project> projects = serviceLocator.getProjectEndpoint().loadJaxbXmlProject(serviceLocator.getSession(), serviceLocator.getUser());
        List<Task> tasks = serviceLocator.getTaskEndpoint().loadJaxbXmlTask(serviceLocator.getSession(), serviceLocator.getUser());
        List<User> users = serviceLocator.getUserEndpoint().loadJaxbXmlUser(serviceLocator.getSession(), serviceLocator.getUser());
        if (projects == null
                || tasks == null
                || users == null) {
            throw new IllegalAccessException();
        }
        System.out.println("ЗАГРУЖЕНО");
    }

    @Override
    public boolean isSecure() {
        assert serviceLocator != null;
        return (!(serviceLocator.getUser() == null));
    }
}
