package com.dolnikova.tm.command.data;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.endpoint.Task;
import com.dolnikova.tm.endpoint.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class DataSaveFasterxmlJsonCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return Command.DATA_SAVE_FASTERXML_JSON;
    }

    @Override
    public @NotNull String description() {
        return Command.DATA_SAVE_FASTERXML_JSON_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        List<User> users = serviceLocator.getUserEndpoint().findAllUser(serviceLocator.getSession());
        List<Project> projects = serviceLocator.getProjectEndpoint().findAllProject(serviceLocator.getSession());
        List<Task> tasks = serviceLocator.getTaskEndpoint().findAllTask(serviceLocator.getSession());
        boolean userSaveAllowed = serviceLocator.getUserEndpoint().saveFasterxmlJsonUser(serviceLocator.getSession(), serviceLocator.getUser(), users);
        if (userSaveAllowed) System.out.println("ПОЛЬЗОВАТЕЛИ СОХРАНЕНЫ");
        boolean taskSaveAllowed = serviceLocator.getTaskEndpoint().saveFasterxmlJsonTask(serviceLocator.getSession(), serviceLocator.getUser(), tasks);
        if (taskSaveAllowed) System.out.println("ЗАДАЧИ СОХРАНЕНЫ");
        boolean projectSaveAllowed = serviceLocator.getProjectEndpoint().saveFasterxmlJsonProject(serviceLocator.getSession(), serviceLocator.getUser(), projects);
        if (projectSaveAllowed) System.out.println("ПРОЕКТЫ СОХРАНЕНЫ");
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUser() == null));
    }
}
