package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Session;
import com.dolnikova.tm.endpoint.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserAuthCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.USER_AUTH;
    }

    @NotNull
    @Override
    public String description() {
        return Command.USER_AUTH_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("[AUTHORIZATION]");
        System.out.println(AdditionalMessage.REG_ENTER_LOGIN);
        @NotNull String login = "";
        while (login.isEmpty()) {
            login = Bootstrap.scanner.nextLine();
        }
        System.out.println(AdditionalMessage.REG_ENTER_PASSWORD);
        @NotNull String password = "";
        while (password.isEmpty()) {
            password = Bootstrap.scanner.nextLine();
        }
        @Nullable final Session session = serviceLocator.getSessionEndpoint().getSession(login, password);
        if (session == null) {
            System.out.println("Не найдена сессия. Отмена.");
            return;
        }
        System.out.println("Получена сессия: " + session.getUserId());
        @Nullable final User currentUser = serviceLocator.getUserEndpoint().findOneBySession(session);
        if (currentUser == null) {
            System.out.println("Не найден пользователь. Отмена.");
            return;
        }
        System.out.println("Получен пользователь: " + currentUser.getLogin());
        serviceLocator.setSession(session);
        serviceLocator.setUser(currentUser);
        System.out.println(AdditionalMessage.REG_USER_SUCCESSFUL_LOGIN);
    }

    @Override
    public boolean isSecure() {
        return serviceLocator.getUser() == null;
    }
}
