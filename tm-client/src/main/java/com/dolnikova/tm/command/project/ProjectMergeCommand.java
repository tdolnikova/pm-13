package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.DataType;
import com.dolnikova.tm.endpoint.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectMergeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.MERGE_PROJECT;
    }

    @NotNull
    @Override
    public String description() {
        return Command.MERGE_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(AdditionalMessage.INSERT_PROJECT_NAME);
        @Nullable Project foundProject = null;
        while (foundProject == null) {
            @NotNull final String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) return;
            foundProject = serviceLocator.getProjectEndpoint().findOneByNameProject(serviceLocator.getSession(), projectName);
            if (foundProject == null) System.out.println(AdditionalMessage.PROJECT_NAME_DOESNT_EXIST + AdditionalMessage.TRY_AGAIN);
            else {
                System.out.println(AdditionalMessage.INSERT_NEW_PROJECT_NAME);
                boolean newNameInserted = false;
                while (!newNameInserted) {
                    @NotNull final String newData = Bootstrap.scanner.nextLine();
                    if (newData.isEmpty()) return;
                    serviceLocator.getProjectEndpoint().mergeProject(
                            serviceLocator.getSession(),
                            newData,
                            foundProject,
                            DataType.NAME);
                    System.out.println(AdditionalMessage.PROJECT_UPDATED);
                    newNameInserted = true;
                }
            }
        }
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUser() == null));
    }
}
