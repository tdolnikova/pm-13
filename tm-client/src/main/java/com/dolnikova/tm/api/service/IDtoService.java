package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.EntityDTO;

public interface IDtoService {

    void saveBin(EntityDTO dto) throws Exception;

    void saveFasterxmlJson(EntityDTO dto) throws Exception;

    void saveFasterxmlXml(EntityDTO dto) throws Exception;

    void saveJaxbJson(EntityDTO dto) throws Exception;

    void saveJaxbXml(EntityDTO dto) throws Exception;

    EntityDTO loadBin() throws Exception;

    EntityDTO loadFasterxmlJson() throws Exception;

    EntityDTO loadFasterxmlXml() throws Exception;

    EntityDTO loadJaxbJson() throws Exception;

    EntityDTO loadJaxbXml() throws Exception;
}