package com.dolnikova.tm.api;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.endpoint.*;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ServiceLocator {
    @Nullable Collection<AbstractCommand> getCommands();
    @Nullable TaskEndpoint getTaskEndpoint();
    @Nullable ProjectEndpoint getProjectEndpoint();
    @Nullable UserEndpoint getUserEndpoint();
    @Nullable SessionEndpoint getSessionEndpoint();
    @Nullable Session getSession();
    void setSession(@Nullable final Session session);
    @Nullable User getUser();
    void setUser(@Nullable final User user);
}
