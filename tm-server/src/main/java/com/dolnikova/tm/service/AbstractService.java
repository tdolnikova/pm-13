package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IRepository;
import com.dolnikova.tm.api.service.IService;
import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.enumerated.DataType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull private IRepository<E> abstractRepository;

    AbstractService(@NotNull final IRepository<E> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Override
    public void saveBin(@Nullable List<E> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        abstractRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable List<E> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        abstractRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable List<E> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        abstractRepository.saveFasterxmlXml(entities);
    }

    @Override
    public void saveJaxbJson(@Nullable List<E> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        abstractRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable List<E> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        abstractRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable List<E> loadBin() throws Exception {
        return abstractRepository.loadBin();
    }

    @Override
    public @Nullable List<E> loadFasterxmlJson() throws Exception {
        return abstractRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable List<E> loadFasterxmlXml() throws Exception {
        return abstractRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable List<E> loadJaxbJson() throws Exception {
        return abstractRepository.loadJaxbJson();
    }

    @Override
    public @Nullable List<E> loadJaxbXml() throws Exception {
        return abstractRepository.loadJaxbXml();
    }
}
