package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.util.PasswordHashUtil;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    private IUserRepository userRepository;
    @Setter private SqlSession sqlSession;

    public UserService(@NotNull final IUserRepository taskRepository) {
        super(taskRepository);
        this.userRepository = taskRepository;
    }

    @Nullable
    @Override
    public User findOneById(@Nullable final String ownerId, @Nullable final String id) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @Nullable User user = null;
        try {
            user = userRepository.findOneByUserId(id);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return user;
    }

    @Override
    public User findOneByLogin(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        @Nullable User user = null;
        try {
            user = userRepository.findOneByLogin(name);
            if (user == null) return null;
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return user;
    }

    @Override
    public User findOneBySession(@Nullable final Session session) {
        if (session == null) return null;
        @Nullable User user = null;
        try {
            user = userRepository.findOneBySession(session);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return user;
    }

    @Override
    public @Nullable List<User> findAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        @Nullable List<User> result = null;
        try {
            result = userRepository.findAll(ownerId);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return result;
    }

    @Override
    public @Nullable final List<User> findAllByLogin(@Nullable final String ownerId, @Nullable final String login) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (login == null || login.isEmpty()) return null;
        @Nullable List<User> result = null;
        try {
            result = userRepository.findAllByLogin(ownerId, login);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return result;
    }

    @Override
    public void persist(@Nullable final User entity) {
        if (entity == null) return;
        @Nullable final String password = entity.getPasswordHash();
        if (password == null) return;
        String hashedPassword = PasswordHashUtil.md5(password);
        entity.setPasswordHash(hashedPassword);
        try {
            userRepository.persist(entity);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public void persistList(@Nullable final List<User> list) {
        if (list == null || list.isEmpty()) return;
        for (User user : list) {
            persist(user);
        }
    }

    @Override
    public void merge(@Nullable final String newData,
                      @Nullable final User entityToMerge,
                      @Nullable final DataType dataType) {
        if (newData == null || newData.isEmpty()) return;
        if (entityToMerge == null || dataType == null) return;
        userRepository.merge(newData, entityToMerge, dataType);
        switch (dataType) {
            case LOGIN:
                updateLogin(newData, entityToMerge);
                break;
            case ROLE:
                updateRole(newData, entityToMerge);
                break;
            case PASSWORD:
                updatePassword(newData, entityToMerge);
                break;
        }
    }

    private void updatePassword(@NotNull final String newData, @NotNull final User entityToMerge) {
        try {
            String id = entityToMerge.getId();
            if (id == null || id.isEmpty()) return;
            userRepository.updatePassword(newData, entityToMerge);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    private void updateLogin(@NotNull final String newData, @NotNull final User entityToMerge) {
        try {
            String id = entityToMerge.getId();
            if (id == null || id.isEmpty()) return;
            userRepository.updateLogin(newData, entityToMerge);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    private void updateRole(@NotNull final String newData, @NotNull final User entityToMerge) {
        try {
            String id = entityToMerge.getId();
            if (id == null || id.isEmpty()) return;
            userRepository.updateRole(newData, entityToMerge);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public void remove(@Nullable final User entity) {
        if (entity == null) return;
        try {
            userRepository.remove(entity);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public void removeAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return;
        try {
            userRepository.removeAll(ownerId);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public boolean checkPassword(@Nullable final String userId, @Nullable final String userInput) {
        if (userId == null || userId.isEmpty()) return false;
        if (userInput == null || userInput.isEmpty()) return false;
        return userRepository.checkPassword(userId, userInput);
    }

    @Override
    public void saveBin(@Nullable final List<User> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        userRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable final List<User> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        userRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable final List<User> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        userRepository.saveFasterxmlXml(entities);
    }

    @Override
    public void saveJaxbJson(@Nullable final List<User> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        userRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable final List<User> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        userRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable final List<User> loadBin() throws Exception {
        return userRepository.loadBin();
    }

    @Override
    public @Nullable final List<User> loadFasterxmlJson() throws Exception {
        return userRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable final List<User> loadFasterxmlXml() throws Exception {
        return userRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable final List<User> loadJaxbJson() throws Exception {
        return userRepository.loadJaxbJson();
    }

    @Override
    public @Nullable final List<User> loadJaxbXml() throws Exception {
        return userRepository.loadJaxbXml();
    }

}
