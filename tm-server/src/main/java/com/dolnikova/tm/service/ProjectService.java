package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.enumerated.DataType;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private IProjectRepository projectRepository;
    @Setter
    private SqlSession sqlSession;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String ownerId, @Nullable final String id) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @Nullable Project project = null;
        try {
            project = projectRepository.findOneByUserId(id);
            if (project == null) return null;
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return project;
    }

    @Override
    public Project findOneByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        @Nullable Project project = null;
        try {
            project = projectRepository.findOneByName(name);
            if (project == null) return null;
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return project;
    }

    @Override
    public @Nullable final List<Project> findAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        @Nullable List<Project> result = null;
        try {
            result = projectRepository.findAll(ownerId);
            if (result == null || result.isEmpty()) return result;
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return result;
    }

    @Override
    public @Nullable final List<Project> findAllByName(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        @Nullable List<Project> result = null;
        try {
            result = projectRepository.findAllByName(ownerId, text);
            if (result == null || result.isEmpty()) return result;
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return result;
    }

    @Override
    public @Nullable final List<Project> findAllByDescription(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        @Nullable List<Project> result = null;
        try {
            result = projectRepository.findAllByDescription(ownerId, text);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return result;
    }

    @Override
    public void persist(@Nullable final Project entity) {
        if (entity == null) return;
        try {
            projectRepository.persist(entity);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public void persistList(@Nullable final List<Project> list) {
        if (list == null || list.isEmpty()) return;
        for (Project project : list) {
            persist(project);
        }
    }

    @Override
    public void merge(@Nullable final String newData,
                      @Nullable final Project entityToMerge,
                      @Nullable final DataType dataType) {
        if (newData == null || newData.isEmpty()) return;
        if (entityToMerge == null || dataType == null) return;
        switch (dataType) {
            case NAME:
                updateName(newData, entityToMerge);
                break;
            case DESCRIPTION:
                updateDescription(newData, entityToMerge);
                break;
        }
    }

    private void updateName(@NotNull final String newData, @NotNull final Project entityToMerge) {
        try {
            projectRepository.updateName(newData, entityToMerge);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    private void updateDescription(@NotNull final String newData, @NotNull final Project entityToMerge) {
        try {
            projectRepository.updateDescription(newData, entityToMerge);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public void remove(@Nullable final Project entity) {
        if (entity == null) return;
        try {
            projectRepository.remove(entity);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public void removeAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return;
        try {
            projectRepository.removeAll(ownerId);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public void saveBin(@Nullable final List<Project> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        projectRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable final List<Project> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        projectRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable final List<Project> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        projectRepository.saveFasterxmlXml(entities);
        System.out.println(this.getClass().getName());
    }

    @Override
    public void saveJaxbJson(@Nullable final List<Project> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        projectRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable final List<Project> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        projectRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable final List<Project> loadBin() throws Exception {
        return projectRepository.loadBin();
    }

    @Override
    public @Nullable final List<Project> loadFasterxmlJson() throws Exception {
        return projectRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable final List<Project> loadFasterxmlXml() throws Exception {
        return projectRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable final List<Project> loadJaxbJson() throws Exception {
        return projectRepository.loadJaxbJson();
    }

    @Override
    public @Nullable final List<Project> loadJaxbXml() throws Exception {
        return projectRepository.loadJaxbXml();
    }

}
