package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.DataType;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public final class TaskService extends AbstractService<Task> implements ITaskService {

    private ITaskRepository taskRepository;
    @Setter
    private SqlSession sqlSession;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable final String ownerId, @Nullable final String id) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @Nullable Task task = null;
        try {
            task = taskRepository.findOneByUserId(id);
            if (task == null) return null;
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return task;
    }

    @Override
    public Task findOneByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        @Nullable Task task = null;
        try {
            task = taskRepository.findOneByName(name);
            if (task == null) return null;
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return task;
    }

    @Override
    public @Nullable List<Task> findAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        @NotNull List<Task> result = null;
        try {
            result = taskRepository.findAll(ownerId);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return result;
    }

    @Override
    public @Nullable List<Task> findAllByName(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        @NotNull List<Task> result = null;
        try {
            result = taskRepository.findAllByName(ownerId, text);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return result;
    }

    @Override
    public @Nullable List<Task> findAllByDescription(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        @NotNull List<Task> result = null;
        try {
            result = taskRepository.findAllByDescription(ownerId, text);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return result;
    }

    @Override
    public void persist(@Nullable final Task entity) {
        if (entity == null) return;
        try {
            taskRepository.persist(entity);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public void persistList(@Nullable final List<Task> list) {
        if (list == null || list.isEmpty()) return;
        for (Task task : list) {
            persist(task);
        }
    }

    @Override
    public void merge(@Nullable final String newData,
                      @Nullable final Task entityToMerge,
                      @Nullable final DataType dataType) {
        if (newData == null || newData.isEmpty()) return;
        if (entityToMerge == null || dataType == null) return;
        switch (dataType) {
            case NAME:
                updateName(newData, entityToMerge);
                break;
            case DESCRIPTION:
                updateDescription(newData, entityToMerge);
                break;
        }
    }

    private void updateName(@NotNull final String newData, @NotNull final Task entityToMerge) {
        try {
            taskRepository.updateName(newData, entityToMerge);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    private void updateDescription(@NotNull final String newData, @NotNull final Task entityToMerge) {
        try {
            taskRepository.updateDescription(newData, entityToMerge);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public void remove(@Nullable final Task entity) {
        if (entity == null) return;
        try {
            taskRepository.remove(entity);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public void removeAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return;
        try {
            taskRepository.removeAll(ownerId);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public void saveBin(@Nullable final List<Task> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        taskRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable List<Task> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        taskRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable final List<Task> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        taskRepository.saveFasterxmlXml(entities);
    }

    @Override
    public void saveJaxbJson(@Nullable final List<Task> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        taskRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable final List<Task> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        taskRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable List<Task> loadBin() throws Exception {
        return taskRepository.loadBin();
    }

    @Override
    public @Nullable List<Task> loadFasterxmlJson() throws Exception {
        return taskRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable List<Task> loadFasterxmlXml() throws Exception {
        return taskRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable List<Task> loadJaxbJson() throws Exception {
        return taskRepository.loadJaxbJson();
    }

    @Override
    public @Nullable List<Task> loadJaxbXml() throws Exception {
        return taskRepository.loadJaxbXml();
    }

}
