package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.ISessionRepository;
import com.dolnikova.tm.api.service.ISessionService;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.util.SignatureUtil;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public final class SessionService  extends AbstractService<Session> implements ISessionService {

    private ISessionRepository sessionRepository;
    @Setter
    private SqlSession sqlSession;

    public SessionService(@NotNull final ISessionRepository sessionRepository) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
    }

    @Override
    public void createSession(@Nullable final User user) {
        Session session = new Session();
        session.setUserId(user.getId());
        @Nullable final String signature = SignatureUtil.sign(user.getPasswordHash(), "JAVA", 3);
        System.out.println("Создание. Сигнатура " + user.getLogin() + ": " + signature);
        session.setSignature(signature);
        persist(session);
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable final String ownerId, @Nullable final String id) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @Nullable Session session = null;
        try {
            session = sessionRepository.findOneById(ownerId, id);
            if (session == null) return null;
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return session;
    }

    @Nullable
    @Override
    public Session findOneByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        @Nullable Session session = null;
        try {
            session = sessionRepository.findOneByUserId(userId);
            if (session == null) return null;
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return session;
    }

    @Nullable
    public Session findOneBySignature(@Nullable final String signature) {
        if (signature == null || signature.isEmpty()) return null;
        System.out.println("Поиск сессии в сервисе");
        @Nullable Session session = null;
        try {
            session = sessionRepository.findOneBySignature(signature);
            if (session == null) return null;
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return session;
    }

    @Override
    public @Nullable List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull List<Session> result = null;
        try {
            result = sessionRepository.findAll(userId);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return result;
    }

    @Override
    public void persist(@Nullable final Session entity) {
        if (entity == null) return;
        try {
            sessionRepository.persist(entity);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public void persistList(@Nullable final List<Session> list) {
        if (list == null || list.isEmpty()) return;
        for (Session session : list) {
            persist(session);
        }
    }

    @Override
    public void remove(@Nullable final Session entity) {
        if (entity == null) return;
        try {
            sessionRepository.remove(entity);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        try {
            sessionRepository.removeAll(userId);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
    }

    @Override
    public void saveBin(@Nullable final List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable final List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable final List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveFasterxmlXml(entities);
    }

    @Override
    public void saveJaxbJson(@Nullable final List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable final List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable List<Session> loadBin() throws Exception {
        return sessionRepository.loadBin();
    }

    @Override
    public @Nullable List<Session> loadFasterxmlJson() throws Exception {
        return sessionRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable List<Session> loadFasterxmlXml() throws Exception {
        return sessionRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable List<Session> loadJaxbJson() throws Exception {
        return sessionRepository.loadJaxbJson();
    }

    @Override
    public @Nullable List<Session> loadJaxbXml() throws Exception {
        return sessionRepository.loadJaxbXml();
    }

}
