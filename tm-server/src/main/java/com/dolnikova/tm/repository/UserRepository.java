package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.mapper.UserMapper;
import com.dolnikova.tm.util.PasswordHashUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Setter
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {
    @NotNull
    private UserMapper userMapper;

    @Nullable
    @Override
    public User findOneByUserId(@NotNull final String id) {
        return userMapper.findOneByUserId(id);
    }

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) {
        return userMapper.findOneByLogin(login);
    }

    @Override
    public @Nullable User findOneBySession(@NotNull final Session session) {
        @Nullable final String userId = session.getUserId();
        if (userId == null || userId.isEmpty()) return null;
        return findOneByUserId(session.getUserId());
    }

    @Override
    public @Nullable List<User> findAll(@NotNull final String userId) {
        return userMapper.findAll();
    }

    @Override
    public @Nullable List<User> findAllByLogin(@NotNull final String userId, @NotNull final String text) {
        return userMapper.findAllByLogin(text);
    }

    @Override
    public void persist(@NotNull final User entity) {
        @Nullable final String id = entity.getId();
        @Nullable final String login = entity.getLogin();
        @Nullable final String passwordHash = entity.getPasswordHash();
        if (id == null || id.isEmpty()) return;
        if (login == null || login.isEmpty()) return;
        if (passwordHash == null || passwordHash.isEmpty()) return;
        userMapper.persist(id, login, passwordHash);
    }

    @Override
    public void persistList(@NotNull final List<User> list) {
        for (User user : list) {
            persist(user);
        }
    }

    @Override
    public void merge(@NotNull final String newData, @NotNull final User entityToMerge, @NotNull final DataType dataType) {

    }

    @Override
    public void updatePassword(@NotNull final String newData, @NotNull final User entityToMerge) {
        @Nullable final String id = entityToMerge.getId();
        if (id == null || id.isEmpty()) return;
        userMapper.updatePassword(newData, id);
    }

    @Override
    public void updateLogin(@NotNull final String newData, @NotNull final User entityToMerge) {
        @Nullable final String id = entityToMerge.getId();
        if (id == null || id.isEmpty()) return;
        userMapper.updateLogin(newData, id);
    }

    @Override
    public void updateRole(@NotNull final String newData, @NotNull final User entityToMerge) {
        @Nullable final String id = entityToMerge.getId();
        if (id == null || id.isEmpty()) return;
        userMapper.updateRole(newData, id);
    }

    @Override
    public void remove(@NotNull final User entity) {
        @Nullable final String id = entity.getId();
        if (id == null || id.isEmpty()) return;
        userMapper.remove(id);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        userMapper.removeAll();
    }

    @Override
    public boolean checkPassword(@NotNull final String userId, @NotNull final String userInput) {
        @Nullable final String hashedInput = PasswordHashUtil.md5(userInput);
        System.out.println("Пользовательский пароль: " + userInput);
        System.out.println("Хэшированный пароль: " + hashedInput);
        @Nullable final User user = findOneByUserId(userId);
        if (user != null) {
            @Nullable final String password = user.getPasswordHash();
            System.out.println("Пароль пользователя: " + password);
            return hashedInput.equals(password);
        }
        return false;
    }

    @Override
    public void saveBin(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(new ArrayList<>(entities));
        }
    }

    @Override
    public void saveFasterxmlJson(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveFasterxmlXml(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveJaxbJson(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Override
    public void saveJaxbXml(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Nullable
    @Override
    public List<User> loadBin() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_BIN);
        try (@Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final ArrayList<User> users = (ArrayList) objectInputStream.readObject();
            return users;
        }
    }

    @Nullable
    @Override
    public List<User> loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ArrayList<User> users = objectMapper.readValue(file, ArrayList.class);
        return users;
    }

    @Nullable
    @Override
    public List<User> loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final ArrayList<User> users = xmlMapper.readValue(file, ArrayList.class);
        return users;
    }

    @Nullable
    @Override
    public List<User> loadJaxbJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_JSON);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<User> users = (ArrayList<User>) unmarshaller.unmarshal(file);
        return users;
    }

    @Nullable
    @Override
    public List<User> loadJaxbXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_XML);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<User> users = (ArrayList<User>) unmarshaller.unmarshal(file);
        return users;
    }

    /*private User fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString(DbConstant.ID));
        user.setEmail(row.getString(DbConstant.EMAIL));
        user.setLogin(row.getString(DbConstant.FIRST_NAME));
        user.setLastName(row.getString(DbConstant.LAST_NAME));
        user.setLogin(row.getString(DbConstant.LOGIN));
        user.setMiddleName(row.getString(DbConstant.MIDDLE_NAME));
        user.setPasswordHash(row.getString(DbConstant.PASSWORD_HASH));
        user.setPhone(row.getString(DbConstant.PHONE));
        String role = row.getString(DbConstant.ROLE);
        if (role != null && !role.isEmpty()) user.setRole(Role.valueOf(role));
        //user.setLocked((row.getInt(DbConstant.LOCKED) > 0));
        return user;
    }*/

}
