package com.dolnikova.tm.repository;

import com.dolnikova.tm.Application;
import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.constant.DbConstant;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.mapper.ProjectMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor
@Setter
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    private ProjectMapper projectMapper;

    @Nullable
    @Override
    public Project findOneByUserId(@NotNull final String id) {
        return findOneByUserId(id);
    }

    /*@Nullable
    public Project findOneByUserIdAndId(@NotNull final String userId, @NotNull final String id) {
        @Nullable Project project = null;
        try {
            project = projectMapper.findOneByUserIdAndId(userId, id);
            if (project == null) return null;
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return project;
    }*/

    @Override
    public Project findOneByName(@NotNull final String name) {
        return projectMapper.findOneByName(name);
    }

    @Override
    public @Nullable List<Project> findAll(@NotNull final String userId) {
        return projectMapper.findAll(userId);
    }

    @Override
    public @Nullable List<Project> findAllByName(@NotNull final String userId, @NotNull final String text) {
        return projectMapper.findAllByName(userId, text);
    }

    @Override
    public @Nullable List<Project> findAllByDescription(@NotNull final String userId, @NotNull final String text) {
        return projectMapper.findAllByDescription(userId, text);
    }

    @Override
    public void persist(@NotNull final Project entity) {
        @Nullable final String id = entity.getId();
        @Nullable final String name = entity.getName();
        @Nullable final String userId = entity.getUserId();
        if (id == null || id.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        projectMapper.persist(id, name, userId);
    }

    @Override
    public void persistList(@NotNull final List<Project> list) {
        for (Project project : list) {
            persist(project);
        }
    }

    public void merge(@NotNull final String newData, @NotNull final Project entityToMerge, @NotNull final DataType dataType) {

    }

    public void updateName(@NotNull final String newData, @NotNull final Project entityToMerge) {
        projectMapper.updateName(newData, entityToMerge.getId());
    }

    public void updateDescription(@NotNull final String newData, @NotNull final Project entityToMerge) {
        projectMapper.updateDescription(newData, entityToMerge.getId());
    }

    @Override
    public void remove(@NotNull final Project entity) {
        projectMapper.remove(entity.getId());
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        projectMapper.removeAll(userId);
    }

    @Override
    public void saveBin(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(new ArrayList<>(entities));
        }
    }

    @Override
    public void saveFasterxmlJson(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveFasterxmlXml(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
        System.out.println(this.getClass().getName());
    }

    @Override
    public void saveJaxbJson(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Override
    public void saveJaxbXml(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Nullable
    @Override
    public List<Project> loadBin() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_BIN);
        try (@Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final ArrayList<Project> projects = (ArrayList) objectInputStream.readObject();
            return projects;
        }
    }

    @Nullable
    @Override
    public List<Project> loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ArrayList<Project> projects = objectMapper.readValue(file, ArrayList.class);
        return projects;
    }

    @Nullable
    @Override
    public List<Project> loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final ArrayList<Project> projects = xmlMapper.readValue(file, ArrayList.class);
        return projects;
    }

    @Nullable
    @Override
    public List<Project> loadJaxbJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Project> projects = (ArrayList<Project>) unmarshaller.unmarshal(file);
        return projects;
    }

    @Nullable
    @Override
    public List<Project> loadJaxbXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_XML);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Project> projects = (ArrayList<Project>) unmarshaller.unmarshal(file);
        return projects;
    }

}
