package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IRepository;
import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.mapper.ProjectMapper;
import com.dolnikova.tm.util.PasswordHashUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

}
