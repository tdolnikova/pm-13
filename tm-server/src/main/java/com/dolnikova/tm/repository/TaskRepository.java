package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.mapper.TaskMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.validation.constraints.Null;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Setter
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    private TaskMapper taskMapper;

    @Nullable
    @Override
    public Task findOneByUserId(@NotNull final String id) {
        return taskMapper.findOneByUserId(id);
    }

    /*@Nullable
    public Task findOneByUserIdAndId(@NotNull final String userId, @NotNull final String id) {
        @Nullable Task task = null;
        try {
            task = taskMapper.findOneByUserIdAndId(userId, id);
            if (task == null) return null;
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        }
        return task;
    }*/

    @Override
    public Task findOneByName(@NotNull final String name) {
        return taskMapper.findOneByName(name);
    }

    @Override
    public @Nullable List<Task> findAll(@NotNull final String userId) {
        return taskMapper.findAll(userId);
    }

    @Override
    public @Nullable List<Task> findAllByName(@NotNull final String userId, @NotNull final String text) {
        return taskMapper.findAllByName(userId, text);
    }

    @Override
    public @Nullable List<Task> findAllByDescription(@NotNull final String userId, @NotNull final String text) {
        return taskMapper.findAllByDescription(userId, text);
    }

    @Override
    public void persist(@NotNull final Task entity) {
        @Nullable final String id = entity.getId();
        @Nullable final String name = entity.getName();
        @Nullable final String projectId = entity.getProjectId();
        @Nullable final String userId = entity.getUserId();
        if (id == null || id.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        taskMapper.persist(id, name, projectId, userId);
    }

    @Override
    public void persistList(@NotNull final List<Task> list) {
        for (Task task : list) {
            persist(task);
        }
    }

    public void merge(@NotNull final String newData, @NotNull final Task entityToMerge, @NotNull final DataType dataType) {

    }

    public void updateName(@NotNull final String newData, @NotNull final Task entityToMerge) {
        @Nullable final String id = entityToMerge.getId();
        if (id == null || id.isEmpty()) return;
        taskMapper.updateName(newData, id);
    }

    public void updateDescription(@NotNull final String newData, @NotNull Task entityToMerge) {
        @Nullable final String id = entityToMerge.getId();
        if (id == null || id.isEmpty()) return;
        taskMapper.updateDescription(newData, id);
    }

    @Override
    public void remove(@NotNull final Task entity) {
        String id = entity.getId();
        if (id == null || id.isEmpty()) return;
        taskMapper.remove(id);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        taskMapper.removeAll(userId);
    }

    @Override
    public void saveBin(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(new ArrayList<>(entities));
        }
    }

    @Override
    public void saveFasterxmlJson(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveFasterxmlXml(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveJaxbJson(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Override
    public void saveJaxbXml(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Nullable
    @Override
    public List<Task> loadBin() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_BIN);
        try (@Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final ArrayList<Task> tasks = (ArrayList) objectInputStream.readObject();
            return tasks;
        }
    }

    @Nullable
    @Override
    public List<Task> loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ArrayList<Task> tasks = objectMapper.readValue(file, ArrayList.class);
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final ArrayList<Task> tasks = xmlMapper.readValue(file, ArrayList.class);
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> loadJaxbJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_JSON);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Task> tasks = (ArrayList<Task>) unmarshaller.unmarshal(file);
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> loadJaxbXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_XML);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Task> tasks = (ArrayList<Task>) unmarshaller.unmarshal(file);
        return tasks;
    }

    /*@Nullable
    private Task fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString(DbConstant.ID));
        task.setDateBegin(row.getDate(DbConstant.DATE_BEGIN));
        task.setDateEnd(row.getDate(DbConstant.DATE_END));
        task.setName(row.getString(DbConstant.NAME));
        task.setDescription(row.getString(DbConstant.DESCRIPTION));
        task.setUserId(row.getString(DbConstant.USER_ID));
        task.setProjectId(row.getString(DbConstant.PROJECT_ID));
        return task;
    }*/

}
