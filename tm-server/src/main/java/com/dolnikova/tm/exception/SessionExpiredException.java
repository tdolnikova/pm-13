package com.dolnikova.tm.exception;

public class SessionExpiredException extends Exception {

    public SessionExpiredException() {
        super("Session expired");
    }

}