package com.dolnikova.tm.entity;

import com.dolnikova.tm.enumerated.Role;
import com.dolnikova.tm.util.PasswordHashUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity implements Serializable {

    @Nullable private String id;
    @Nullable private String email;
    @Nullable private String firstName;
    @Nullable private String lastName;
    @Nullable private String login;
    @Nullable private String middleName;
    @Nullable private String passwordHash;
    @Nullable private String phone;
    @Nullable private boolean locked;
    @Nullable private Role role;

}
