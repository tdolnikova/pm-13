package com.dolnikova.tm.entity;

import com.dolnikova.tm.enumerated.Status;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity implements Serializable {

    @Nullable
    private String id = UUID.randomUUID().toString();
    @Nullable
    private Date dateBegin;
    @Nullable
    private Date dateEnd;
    @Nullable
    private String description = "some text";
    @Nullable
    private String name;
    @Nullable
    private String userId;

    @Nullable
    private Date creationDate = new Date();
    @Nullable
    private Status status = Status.PLANNED;

}
