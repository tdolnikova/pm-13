package com.dolnikova.tm.entity;

import com.dolnikova.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity implements Serializable {

    @Nullable private String id = UUID.randomUUID().toString();
    @Nullable private Date dateBegin;
    @Nullable private Date dateEnd;
    @Nullable private String description;
    @Nullable private String name;
    @Nullable private String projectId;
    @Nullable private String userId;

    @Nullable private Date creationDate = new Date();
    @Nullable private Status status = Status.PLANNED;

}
