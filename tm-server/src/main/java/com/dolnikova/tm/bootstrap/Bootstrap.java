package com.dolnikova.tm.bootstrap;


import com.dolnikova.tm.api.ServiceLocator;
import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.api.repository.ISessionRepository;
import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.api.service.ISessionService;
import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.endpoint.ProjectEndpoint;
import com.dolnikova.tm.endpoint.SessionEndpoint;
import com.dolnikova.tm.endpoint.TaskEndpoint;
import com.dolnikova.tm.endpoint.UserEndpoint;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.Role;
import com.dolnikova.tm.mapper.ProjectMapper;
import com.dolnikova.tm.mapper.SessionMapper;
import com.dolnikova.tm.mapper.TaskMapper;
import com.dolnikova.tm.mapper.UserMapper;
import com.dolnikova.tm.repository.ProjectRepository;
import com.dolnikova.tm.repository.SessionRepository;
import com.dolnikova.tm.repository.TaskRepository;
import com.dolnikova.tm.repository.UserRepository;
import com.dolnikova.tm.service.ProjectService;
import com.dolnikova.tm.service.SessionService;
import com.dolnikova.tm.service.TaskService;
import com.dolnikova.tm.service.UserService;
import com.dolnikova.tm.util.DatabaseUtil;
import com.dolnikova.tm.util.DateUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.ws.Endpoint;
import java.io.Reader;
import java.sql.Connection;
import java.util.List;
import java.util.Scanner;

@Getter
@Setter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull private final IUserRepository userRepository = new UserRepository();
    @NotNull private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull private final IUserService userService = new UserService(userRepository);
    @NotNull private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull private final ISessionService sessionService = new SessionService(sessionRepository);

    @Nullable private ProjectMapper projectMapper;
    @Nullable private TaskMapper taskMapper;
    @Nullable private UserMapper userMapper;
    @Nullable private SessionMapper sessionMapper;

    @Nullable
    public static Connection connection;

    public void init() {
        connection = DatabaseUtil.getConnection();
        setMappers();
        start();
    }

    @SneakyThrows
    private void setMappers() {
        Reader reader = Resources.getResourceAsReader(General.MYBATIS_CONFIG);
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory sessionFactory = builder.build(reader);
        SqlSession session = sessionFactory.openSession();
        projectMapper = session.getMapper(ProjectMapper.class);
        projectRepository.setProjectMapper(projectMapper);
        projectService.setSqlSession(session);
        taskMapper = session.getMapper(TaskMapper.class);
        taskRepository.setTaskMapper(taskMapper);
        taskService.setSqlSession(session);
        userMapper = session.getMapper(UserMapper.class);
        userRepository.setUserMapper(userMapper);
        userService.setSqlSession(session);
        sessionMapper = session.getMapper(SessionMapper.class);
        sessionRepository.setSessionMapper(sessionMapper);
        sessionService.setSqlSession(session);
    }

    private void start() {
        Endpoint.publish(General.ADDRESS + "TaskEndpoint?WSDL", new TaskEndpoint(this));
        Endpoint.publish(General.ADDRESS + "ProjectEndpoint?WSDL", new ProjectEndpoint(this));
        Endpoint.publish(General.ADDRESS + "UserEndpoint?WSDL", new UserEndpoint(this));
        Endpoint.publish(General.ADDRESS + "SessionEndpoint?WSDL", new SessionEndpoint(this));

        System.out.println(General.ADDRESS + "TaskEndpoint?WSDL");
        System.out.println(General.ADDRESS + "ProjectEndpoint?WSDL");
        System.out.println(General.ADDRESS + "UserEndpoint?WSDL");
        System.out.println(General.ADDRESS + "SessionEndpoint?WSDL");

        Scanner scanner = new Scanner(System.in);
        String input = "";
        while (!"exit".equals(input)) {
            try {
                input = scanner.nextLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            if (connection != null) connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);
    }

    private void registryUser(@NotNull final User user) {
        if (user.getLogin() == null || user.getLogin().isEmpty()) return;
        if (user.getPasswordHash() == null || user.getPasswordHash().isEmpty()) return;
        if (user.getRole() == null) return;
        userService.persist(user);
        sessionService.createSession(user);
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return sessionService;
    }


    private void insertDullData() {
        @NotNull final User user = new User();
        user.setLogin("user");
        user.setPasswordHash("user");
        user.setRole(Role.USER);
        registryUser(user);

        //userService.setCurrentUser(user);

        @NotNull final User admin = new User();
        admin.setLogin("admin");
        admin.setPasswordHash("admin");
        admin.setRole(Role.ADMIN);
        registryUser(admin);

        @NotNull final Project project = new Project();
        project.setUserId(user.getId());
        project.setName("superproject");
        project.setDateBegin(DateUtil.stringToDate("01.01.1998"));
        project.setDateEnd(DateUtil.stringToDate("12.05.1999"));
        projectService.persist(project);

        @NotNull final Task task1 = new Task();
        task1.setUserId(user.getId());
        task1.setName("task1");
        task1.setProjectId(project.getId());
        task1.setDateBegin(DateUtil.stringToDate("03.11.2019"));
        task1.setDateEnd(DateUtil.stringToDate("03.07.2020"));
        taskService.persist(task1);

        @NotNull final Task task2 = new Task();
        task1.setUserId(user.getId());
        task1.setName("task2");
        task2.setProjectId(project.getId());
        task2.setDateBegin(DateUtil.stringToDate("10.06.1999"));
        task2.setDateEnd(DateUtil.stringToDate("12.08.2000"));
        taskService.persist(task2);

        List<Project> projects = projectService.findAllByName(user.getId(), "hjv");
        for (Project p : projects) {
            System.out.println(project.getName());
        }
    }

}
