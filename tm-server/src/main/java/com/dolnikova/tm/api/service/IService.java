package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.enumerated.DataType;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    void setSqlSession(@Nullable final SqlSession sqlSession);

    @Nullable
    E findOneById(@Nullable final String ownerId, @Nullable final String id);

    @Nullable
    List<E> findAll(@Nullable final String ownerId);

    void persist(@Nullable final E entity);

    void persistList(@Nullable final List<E> list);

    void remove(@Nullable final E entity);

    void removeAll(@Nullable final String ownerId);

    void saveBin(@Nullable final List<E> entities) throws Exception;

    void saveFasterxmlJson(@Nullable final List<E> entities) throws Exception;

    void saveFasterxmlXml(@Nullable final List<E> entities) throws Exception;

    void saveJaxbJson(@Nullable final List<E> entities) throws Exception;

    void saveJaxbXml(@Nullable final List<E> entities) throws Exception;

    @Nullable
    List<E> loadBin() throws Exception;

    @Nullable
    List<E> loadFasterxmlJson() throws Exception;

    @Nullable
    List<E> loadFasterxmlXml() throws Exception;

    @Nullable
    List<E> loadJaxbJson() throws Exception;

    @Nullable
    List<E> loadJaxbXml() throws Exception;

}
