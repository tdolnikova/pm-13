package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.mapper.SessionMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    void setSessionMapper(SessionMapper sessionMapper);

    @Nullable
    @Override
    Session findOneByUserId(final @NotNull String id);

    @Nullable
    Session findOneById(final @Nullable String ownerId, final @Nullable String id);

    @Nullable
    Session findOneBySignature(@Nullable final String signature);

    @Override
    @Nullable List<Session> findAll(final @NotNull String ownerId);

    @Override
    void persist(final @NotNull Session entity);

    @Override
    void persistList(final @NotNull List<Session> list);

    @Override
    void remove(final @NotNull Session entity);

    @Override
    void removeAll(final @NotNull String ownerId);

    @Override
    void saveBin(final @NotNull List<Session> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @NotNull List<Session> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @NotNull List<Session> entities) throws Exception;

    @Override
    void saveJaxbJson(final @NotNull List<Session> entities) throws Exception;

    @Override
    void saveJaxbXml(final @NotNull List<Session> entities) throws Exception;

    @Nullable
    @Override
    List<Session> loadBin() throws Exception;

    @Nullable
    @Override
    List<Session> loadFasterxmlJson() throws Exception;

    @Nullable
    @Override
    List<Session> loadFasterxmlXml() throws Exception;

    @Nullable
    @Override
    List<Session> loadJaxbJson() throws Exception;

    @Nullable
    @Override
    List<Session> loadJaxbXml() throws Exception;
}
