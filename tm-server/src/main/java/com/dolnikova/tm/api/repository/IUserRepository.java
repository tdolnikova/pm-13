package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.mapper.UserMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    void updatePassword(@NotNull final String newData, @NotNull final User entityToMerge);

    void updateLogin(@NotNull final String newData, @NotNull final User entityToMerge);

    void updateRole(@NotNull final String newData, @NotNull final User entityToMerge);

    void setUserMapper(UserMapper userMapper);

    @Nullable
    @Override
    User findOneByUserId(final @NotNull String id);

    User findOneByLogin(final @NotNull String name);

    @Nullable
    User findOneBySession(@NotNull final Session session);

    @Override
    @Nullable List<User> findAll(final @NotNull String ownerId);

    @Nullable List<User> findAllByLogin(@NotNull String ownerId, @NotNull String text);

    @Override
    void persist(final @NotNull User entity);

    @Override
    void persistList(final @NotNull List<User> list);

    void merge(final @NotNull String newData, final @NotNull User entityToMerge, final @NotNull DataType dataType);

    @Override
    void remove(final @NotNull User entity);

    @Override
    void removeAll(final @NotNull String ownerId);

    boolean checkPassword(final @Nullable String userId, final @Nullable String userInput);

    @Override
    void saveBin(final @NotNull List<User> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @NotNull List<User> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @NotNull List<User> entities) throws Exception;

    @Override
    void saveJaxbJson(final @NotNull List<User> entities) throws Exception;

    @Override
    void saveJaxbXml(final @NotNull List<User> entities) throws Exception;

    @Nullable
    @Override
    List<User> loadBin() throws Exception;

    @Nullable
    @Override
    List<User> loadFasterxmlJson() throws Exception;

    @Nullable
    @Override
    List<User> loadFasterxmlXml() throws Exception;

    @Nullable
    @Override
    List<User> loadJaxbJson() throws Exception;

    @Nullable
    @Override
    List<User> loadJaxbXml() throws Exception;
}
