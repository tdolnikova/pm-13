package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.mapper.ProjectMapper;
import com.dolnikova.tm.mapper.SessionMapper;
import com.dolnikova.tm.mapper.TaskMapper;
import com.dolnikova.tm.mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @Nullable
    E findOneByUserId(@NotNull final String id);

    @Nullable
    List<E> findAll(@NotNull final String ownerId);

    void persist(@NotNull final E entity);

    void persistList(@NotNull final List<E> list);

    void remove(@NotNull final E entity);

    void removeAll(@NotNull final String ownerId);

    void saveBin(@NotNull final List<E> entities) throws Exception;

    void saveFasterxmlJson(@NotNull final List<E> entities) throws Exception;

    void saveFasterxmlXml(@NotNull final List<E> entities) throws Exception;

    void saveJaxbJson(@NotNull final List<E> entities) throws Exception;

    void saveJaxbXml(@NotNull final List<E> entities) throws Exception;

    @Nullable
    List<E> loadBin() throws Exception;

    @Nullable
    List<E> loadFasterxmlJson() throws Exception;

    @Nullable
    List<E> loadFasterxmlXml() throws Exception;

    @Nullable
    List<E> loadJaxbJson() throws Exception;

    @Nullable
    List<E> loadJaxbXml() throws Exception;

}
