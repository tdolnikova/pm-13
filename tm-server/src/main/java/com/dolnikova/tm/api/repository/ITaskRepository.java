package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.mapper.TaskMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void updateName(@NotNull String newData, @NotNull Task entityToMerge);

    void updateDescription(@NotNull String newData, @NotNull Task entityToMerge);

    void setTaskMapper(TaskMapper taskMapper);

    @Nullable
    @Override
    Task findOneByUserId(final @NotNull String id);

    Task findOneByName(final @NotNull String name);

    @Override
    @Nullable List<Task> findAll(final @NotNull String ownerId);

    @Nullable List<Task> findAllByName(@NotNull String ownerId, @NotNull String text);

    @Nullable List<Task> findAllByDescription(@NotNull String ownerId, @NotNull String text);

    @Override
    void persist(final @NotNull Task entity);

    @Override
    void persistList(final @NotNull List<Task> list);

    void merge(final @NotNull String newData, final @NotNull Task entityToMerge, final @NotNull DataType dataType);

    @Override
    void remove(final @NotNull Task entity);

    @Override
    void removeAll(final @NotNull String ownerId);

    @Override
    void saveBin(final @NotNull List<Task> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @NotNull List<Task> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @NotNull List<Task> entities) throws Exception;

    @Override
    void saveJaxbJson(final @NotNull List<Task> entities) throws Exception;

    @Override
    void saveJaxbXml(final @NotNull List<Task> entities) throws Exception;

    @Nullable
    @Override
    List<Task> loadBin() throws Exception;

    @Nullable
    @Override
    List<Task> loadFasterxmlJson() throws Exception;

    @Nullable
    @Override
    List<Task> loadFasterxmlXml() throws Exception;

    @Nullable
    @Override
    List<Task> loadJaxbJson() throws Exception;

    @Nullable
    @Override
    List<Task> loadJaxbXml() throws Exception;
}
